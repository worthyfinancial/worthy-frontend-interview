import axios from 'axios'

export default async ({ Vue }) => {
  axios.interceptors.request.use((config) => {
    // Add sandbox Plaid public key to all reqs
    config.data = Object.assign(config.data, {
      'public_key': 'e8d4281b961324b516902953089985'
    })
    return config
  }, (error) => {
    return Promise.reject(error)
  })

  Vue.prototype.$axios = axios
}
