# Worthy Frontend Interview (worthy-frontend-interview)

A boilerplate repo for frontend coding interviews at Worthy Financial. Make sure the following commands all work on your system before the interview.

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
npm run dev
```

### Lint the files
```bash
npm run lint
```

### Run unit tests
```bash
npm run test
```

# Documentation

You will find all you'll need for the UI at https://quasar.dev.