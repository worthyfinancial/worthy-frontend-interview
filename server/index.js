const express = require('express')
const app = express()
app.use(express.json())
const request = require('request')
const port = 3000

app.post('/institutions/get_by_id', (req, res) => {
  request.post('https://development.plaid.com/institutions/get_by_id', {
    json: true,
    body: req.body
  }).pipe(res)
})

app.post('/institutions/search', (req, res) => {
  request.post('https://development.plaid.com/institutions/search', {
    json: true,
    body: req.body
  }).pipe(res)
})

app.listen(port, () => console.log(`Proxy API listening on port ${port}!`))
